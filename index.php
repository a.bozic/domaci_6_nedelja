<!DOCTYPE HTML>

<html>

<head>
  <title>Untitled</title>
</head>

<body>

    <p>Promenljiva tipa string</p>
    <?php
        $color = "red";
        $string = "PHP";
        echo "<span <span style='color:$color'> " .$string.  "</span>" ;
    ?>

    <p>Promenljiva tipa integer</p>
    <?php
        $color = "green";
        $int = 100;
        echo "<span style='color:$color'> " .$int.  "</span>" ;
    ?>

    <p>Promenljiva tipa float</p>
    <?php
        $color = "blue";
        $float = "150.5";
        echo "<span style='color:$color'> " .$float.  "</span>" ;
    ?>

    <p>Promenljiva tipa boolean</p>
    <?php
        $color="deeppink";
        $bool=TRUE;
        echo "<span style='color:$color'> " .$bool. "</span>";
    ?>
    <p>Promenljiva tipa array</p>
    <?php
        $color = "darkblue";
        $array = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        foreach($array as $value){
        	echo "<span style='color:$color'> " .$value. "</span>";
        }
    ?>
    <p>Suma niza</p>
   	<?php
   	      $color = "darkviolet";
   	      $niz = array(10,20,30,40,50,60);
   	      include 'functions.php';
          echo colorSuma($color , $niz);
          echo "<br>";
          echo "Najmanji broj u nizu je ";
          echo colormin($color, $niz);
          echo "<br>";
          echo "Najveći broj u nizu je ";
          echo colormax($color, $niz);
   	   ?>
</body>

</html>